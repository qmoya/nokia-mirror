module.exports = function (grunt) {
    'use strict';

    grunt.loadNpmTasks('grunt-jslint');
    grunt.loadNpmTasks('grunt-contrib-jasmine');

    grunt.initConfig({
        jslint: {
            client: {
                src: [
                    'src/*.js'
                ],
                directives: {
                    browser: true,
                    devel: true
                }
            }
        },
        jasmine: {
            src: [
                'src/*.js'
            ],
            options: {
                specs: [
                    'spec/PlayerSpec.js'
                ],
                helpers: [
                    'spec/SpecHelper.js'
                ]
            }
        }
    });

    grunt.registerTask('default', 'jslint');
    grunt.registerTask('spec', 'jasmine');
};
