/*jslint browser: true, devel: true*/

var button_view = function (spec) {
        'use strict';
        var that = {},
            el = spec.el;
    },
    compassButton =
    ad = function () {
        'use strict';
        var that = {},
            disable_scroll = function () {
                document.addEventListener('touchmove', function () {
                    event.preventDefault();
                });
            },
            hide_address_bar = function () {
                setTimeout(function () {
                    window.scrollTo(0, 1);
                }, 100);
            },
            fade_background = function () {
                document.getElementById('background').style.opacity = '1';
            },
            present_button = function (button, delay) {
                button.style['-webkit-transition'] = 'all 0.5s ease-out ' + delay + 'ms';
                button.style['-webkit-transform'] = 'perspective(600px) rotateY(0deg) scale(1)';
                button.style.opacity = 1;
            },
            show_buttons = function () {
                var buttons = document.getElementsByClassName('button'), i;
                for (i = 0; i < buttons.length; i = i + 1) {
                    present_button(buttons[i], i * 100);
                }
            },
            show = function () {
                disable_scroll();
                hide_address_bar();
                fade_background();
                show_buttons();
            };

        that.show = show;
        return that;
    };

window.addEventListener('load', function () {
    'use strict';
    ad().show();
});
